#include<iostream>
#include<list>
using namespace std;

void printElement(list<int> l){
    for(list<int>::iterator j = l.begin();j != l.end(); j++)
        cout<<" "<< *j;
    cout<<endl;
}


int main(){

    list<int> _list1;
    _list1.push_back(7);
    _list1.push_back(39);
    _list1.push_back(13);
    _list1.push_front(100);
    _list1.push_front(55);
    _list1.push_front(200);

    for(int i = 1; i <= 20; i = i+5)
        _list1.push_back(i);

    cout<<"size of list: "<<_list1.size()<<endl;

    cout<<"list is ";
    printElement(_list1);

    cout<<"list after sorting:";
    _list1.sort();
    printElement(_list1);

    cout<<"list after deleting the first element:";
    _list1.pop_front();
    printElement(_list1);

    cout<<"list after deleting the end element:";
    _list1.pop_back();
    printElement(_list1);

    list<int>::iterator position =_list1.begin(); //=0
    position++;//=1
    _list1.erase(position);
    cout<<"list after deleting... ";  //<<position<<"-th element:";
    printElement(_list1);

    list<int>::iterator position1 = position;
    position1++;

    //delete array
    _list1.erase(position, position1);
    printElement(_list1);

    //insert
    _list1.insert(_list1.begin(), 22);
    printElement(_list1);






    return 0;
}
